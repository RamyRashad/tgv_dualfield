#!/bin/bash

# Set the simulation parameters
export prob_num=4 # for TGV_2D = 1, for TGV_3D = 2, for IVA =3, for ISL = 4
export t_f=1 # final simulation time
export f_sol=10 # frequency of solution

export num_cores=1 # choose number of cores for parallel implementation
export n_mesh_el=4 # choose number of mesh elements per side (integer number >= 3)
export pol_deg=2 # choose polynomial degree (integer number >= 1)

export save_fig="True" # set to True to save results figure
export save_pvd="True"  # set to True to save pvd results for visualization in Paraview

# Run the Python script
mpiexec -n $num_cores python3 navier_stokes/mpi_dualfield_NS.py

# Tip 1: Make sure when exporting environment variables not to add any spaces:
# e.g. prob_num=1 is valid but prob_num = 1 is invalid

# Tip 2: To exit properly after the simulation ends --> Type <exit>.
# Otherwise you might get an error when you run the docker container next time stating that
# the container name "portwings_container" has already been used.

# Tip 3: To activate the virtual environment of firedrake manually, which is not needed, you can use
## source ../firedrake/bin/activate

# Tip 4: To check the packages installed in the firedrake virtual env, use
## pip list