from .problem_base import *
import numpy as np
from math import pi


class InviscidShearLayer(ProblemBase):
    "2D Taylor Green problem."

    def __init__(self, options):
        ProblemBase.__init__(self, options)
        # Quadrilateral mesh
        if options["mesh_type"] == "quad":
            self.quad = True
        else:
            self.quad = False

        self.mesh = PeriodicRectangleMesh(self.n_el, self.n_el, 2 * pi, 2 * pi, direction="both", quadrilateral=self.quad, comm = self.comm)
        self.init_mesh()

        # Set dynamic viscosity
        self.mu = 0.0
        # Set density
        self.rho = 1.0
        # Kinematic viscosity
        self.kappa = self.mu / self.rho
        # Periodic Problem
        self.periodic = True
        # Solution exact
        self.exact = False

    def initial_conditions(self, V_v, V_w, V_p):
        x, y = SpatialCoordinate(self.mesh)

        eps = 0.05
        delta = pi / 15.0
        f1 = (y - 0.5 * pi) / delta
        f2 = (1.5 * pi - y) / delta

        v_x = conditional(le(y, pi), tanh(f1), tanh(f2))
        v_y = eps * sin(x)

        v_0 = as_vector([v_x, v_y])

        w_0_case1 = eps * cos(x) - 1.0 / (delta * (cosh(f1)) ** 2)
        w_0_case2 = eps * cos(x) + 1.0 / (delta * (cosh(f2)) ** 2)

        w_0 = conditional(le(y, pi), w_0_case1, w_0_case2)
        p_0 = Constant(0)

        if not self.quad:
            v_init = interpolate(v_0, V_v)
            w_init = interpolate(w_0, V_w)
            p_init = interpolate(p_0, V_p)
        else:
            v_init = project(v_0, V_v)
            w_init = project(w_0, V_w)
            p_init = project(p_0, V_p)
        return [v_init, w_init, p_init]

    def __str__(self):
        return "Inviscid_Shear_Layer"
