from .problem_base import *
import numpy as np
from math import pi

class TaylorGreen2D(ProblemBase):
    "2D Taylor Green problem."
    def __init__(self, options):
        ProblemBase.__init__(self, options)
        # Quadrilateral mesh
        if options["mesh_type"] == "quad":
            self.quad = True
        else:
            self.quad = False

        self.mesh = PeriodicRectangleMesh(self.n_el, self.n_el, 2, 2, direction="both", quadrilateral=self.quad, comm = self.comm)
        self.mesh.coordinates.dat.data[:, 0] -= 1
        self.mesh.coordinates.dat.data[:, 1] -= 1
        self.init_mesh()

        # Set dynamic viscosity
        self.mu = 1.0 / 100
        # Set density
        self.rho = 1
        # Kinematic viscosity
        self.kappa = self.mu/self.rho
        # Periodic Problem
        self.periodic = True
        # Solution exact
        self.exact = True


    def exact_solution(self, t=0):

        x, y = SpatialCoordinate(self.mesh)
        # t = Constant(time)
        # Mesh coordinates

        v_1 = -cos(pi*x)*sin(pi*y)*exp(-2*(pi**2)*self.mu*t)
        v_2 = sin(pi*x)*cos(pi*y)*exp(-2*(pi**2)*self.mu*t)

        p = -(1/4)*(cos(2*pi*x) + cos(2*pi*y))*exp(-4*(pi**2)*self.mu*t) # This is static pressure
        # FixMe: Add dynamic pressure

        w = 2*pi*cos(pi*x)*cos(pi*y)*exp(-2*(pi**2)*self.mu*t)
        return as_vector([v_1,v_2]), w, p

    def approximate_solution(self, V_v, V_w, V_p, t=0.0):
        if not self.quad:
            v_ex, w_ex, p_ex = self.exact_solution(t)
            v_init = interpolate(v_ex, V_v)
            if V_w is not None:
                w_init = interpolate(w_ex, V_w)
            else:
                w_init = None
            p_init = interpolate(p_ex, V_p)
        else:
            v_ex, w_ex, p_ex = self.exact_solution(t)
            v_init = project(v_ex, V_v)
            if V_w is not None:
                w_init = project(w_ex, V_w)
            else:
                w_init = None
            p_init = project(p_ex, V_p)
        return [v_init, w_init, p_init]

    def initial_conditions(self, V_v, V_w, V_p):
        return self.approximate_solution(V_v, V_w, V_p,0)


    def init_outputs(self, t_c):
        # 6 outputs --> 3 exact states (velocity , vorticity and pressure)
        # and 3 exact integral quantities at time t (energy, enstrophy, helicity)
        u_ex_t, w_ex_t, p_ex_t = self.exact_solution(t_c)
        H_ex_t = 0.5 * (inner(u_ex_t, u_ex_t) * dx(domain=self.mesh))
        E_ex_t = 0.5 * (inner(w_ex_t, w_ex_t) * dx(domain=self.mesh))
        Ch_ex_t = None#(inner(u_ex_t, w_ex_t) * dx(domain=self.mesh))
        return u_ex_t, w_ex_t, p_ex_t,H_ex_t,E_ex_t,Ch_ex_t

    def __str__(self):
        return "Taylor_Green2D"