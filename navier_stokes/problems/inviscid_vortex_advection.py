from .problem_base import *
import numpy as np
from math import pi

class InviscidVortexAdvection(ProblemBase):
    "Inviscid Vortex Advection problem."
    def __init__(self, options):
        ProblemBase.__init__(self, options)
        # Quadrilateral mesh
        if options["mesh_type"] == "quad":
            self.quad = True
        else:
            self.quad = False

        self.mesh = PeriodicRectangleMesh(self.n_el, self.n_el, 20, 20, direction="both", quadrilateral=self.quad, comm = self.comm)
        self.mesh.coordinates.dat.data[:, 0] -= 10
        self.mesh.coordinates.dat.data[:, 1] -= 10
        self.init_mesh()

        # Set dynamic viscosity
        self.mu = 0.0
        # Set density
        self.rho = 1
        # Kinematic viscosity
        self.kappa = self.mu/self.rho
        # Periodic Problem
        self.periodic = True
        # Solution exact
        self.exact = True


    def exact_solution(self, t=0):
        # Mesh coordinates
        x, y = SpatialCoordinate(self.mesh)
        # t = Constant(time)

        C = 0.6944 # Vortex strengh parameter
        r_squared = (x-t)*(x-t) + (y-t)*(y-t)
        v_1 = 1 - C * (y - t) * exp(-0.5 * r_squared)
        v_2 = 1 + C * (x - t) * exp(-0.5 * r_squared)
        p = C*C*exp(-r_squared)
        w = C*(2 - r_squared)*exp(-0.5 * r_squared)

        return as_vector([v_1,v_2]), w, p

    def approximate_solution(self, V_v, V_w, V_p, t=0.0):
        if not self.quad:
            v_ex, w_ex, p_ex = self.exact_solution(t)
            v_init = interpolate(v_ex, V_v)
            if V_w is not None:
                w_init = interpolate(w_ex, V_w)
            else:
                w_init = None
            p_init = interpolate(p_ex, V_p)
        else:
            v_ex, w_ex, p_ex = self.exact_solution(t)
            v_init = project(v_ex, V_v)
            if V_w is not None:
                w_init = project(w_ex, V_w)
            else:
                w_init = None
            p_init = project(p_ex, V_p)
        return [v_init, w_init, p_init]

    def initial_conditions(self, V_v, V_w, V_p):
        return self.approximate_solution(V_v, V_w, V_p,0)


    def init_outputs(self, t_c):
        # 6 outputs --> 3 exact states (velocity , vorticity and pressure)
        # and 3 exact integral quantities at time t (energy, enstrophy, helicity)
        u_ex_t, w_ex_t, p_ex_t = self.exact_solution(t_c)
        H_ex_t = 0.5 * (inner(u_ex_t, u_ex_t) * dx(domain=self.mesh))
        E_ex_t = 0.5 * (inner(w_ex_t, w_ex_t) * dx(domain=self.mesh))
        Ch_ex_t = None#(inner(u_ex_t, w_ex_t) * dx(domain=self.mesh))
        return u_ex_t, w_ex_t, p_ex_t,H_ex_t,E_ex_t,Ch_ex_t

    def __str__(self):
        return "Inviscid_Vortex_Advection"