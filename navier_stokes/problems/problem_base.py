from firedrake import *

from numpy import linspace, array
import matplotlib.pyplot as plt
from matplotlib import interactive
from firedrake.petsc import PETSc


class ProblemBase:
    "Base class for all problems."

    def __init__(self, options):

        # Store options
        self.options = options
        self.comm = options["comm"]

        self.n_el = options["n_mesh_el"]  # Number of spatial elements
        self.t_fin = options["t_fin"]  # Final time
        self.n_t = options["n_t_steps"]  # Number of temporal elements

        # Parameters must be defined by subclass
        self.mesh = None
        self.dimM = None
        self.n_ver = None
        self.dt = None
        self.t_vec = None
        self.quad = False
        self.output_location = ''

        self.mesh_info_arr = None

    def init_mesh(self,show_mesh = False):
        #PETSc.Sys.Print('1- setting up mesh across %d processes' % self.mesh.comm.size)

        self.mesh.init()
        self.dimM = self.mesh.geometric_dimension()

        # PETSc.Sys.Print('  rank %d owns %d elements and can access %d faces, %d edges, and %d vertices' \
        #                 % (self.mesh.comm.rank, self.mesh.num_cells(), self.mesh.num_faces(), self.mesh.num_edges(), self.mesh.num_vertices()),
        #                 comm=COMM_SELF)

        mesh_info = np.array([self.mesh.num_cells(), self.mesh.num_faces(), self.mesh.num_edges(), self.mesh.num_vertices()])
        # Gather all arrays in rank 0
        all_arrays = self.comm.gather(mesh_info, root=0)
        if self.comm.Get_rank() == 0:
            self.mesh_info_arr = np.vstack(all_arrays)
            #print(f"Initialized Mesh with [cells, faces, edges ,vertices]")
            #print(self.mesh_info_arr)
        self.n_ver = FacetNormal(self.mesh)

        if show_mesh:
            #plt.ion()
            fig, axes = plt.subplots()
            triplot(self.mesh, axes=axes)
            axes.legend()
            fig.show()

            plt.savefig('mesh.png')
            #plt.show()

        #plt.show(block=False)

    def structured_time_grid(self):
        self.dt = self.t_fin / self.n_t
        self.t_vec = linspace(0, self.t_fin, self.n_t + 1)

    def update_problem(self, t):
        "Update problem-specific at time t"
        pass


    def initial_conditions(self, V, Q):
        pass

    def boundary_conditions(self, t_c):
        # no boundary conditions
        return None

    def init_outputs(self, t_c):
        # no outputs
        return []

    def calculate_outputs(self,exact_arr,u_t,w_t,p_t):
        return array([])
