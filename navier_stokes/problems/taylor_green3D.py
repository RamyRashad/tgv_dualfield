from .problem_base import *
import numpy as np
from math import pi
import sympy as sym

class TaylorGreen3D(ProblemBase):
    "3D Taylor Green problem."
    def __init__(self, options):
        ProblemBase.__init__(self, options)

        if options["mesh_type"] == "quad":
            self.quad = True
            # Quad mesh (Firedrake does not support periodic hexahedral meshes, this is a hack to do it)
            layer = 2 * pi / self.n_el
            mesh_2d = PeriodicRectangleMesh(self.n_el, self.n_el, Lx=2 * pi, Ly=2 * pi, quadrilateral=True, comm = self.comm)
            self.mesh = ExtrudedMesh(mesh_2d, self.n_el, layer_height=layer, periodic=True)
        else:
            self.quad = False
            self.mesh = PeriodicBoxMesh(self.n_el, self.n_el, self.n_el, 2 * pi, 2 * pi, 2 * pi, comm=self.comm)

        # Translate mesh so that left down corner (-pi,-pi,-pi)
        self.mesh.coordinates.dat.data[:, 0] -= pi
        self.mesh.coordinates.dat.data[:, 1] -= pi
        self.mesh.coordinates.dat.data[:, 2] -= pi
        self.init_mesh()

        # Set kinematic viscosity
        self.mu = 1.0 / 400
        # Set density
        self.rho = 1
        # Dynamic viscosity
        self.kappa = self.mu/self.rho
        # Periodic problem
        self.periodic = True
        # Solution is not exact
        self.exact = False


    def initial_conditions(self, V_v, V_w, V_p):
        # FixMe: Make sure solution is correct

        x, y, z = SpatialCoordinate(self.mesh)
        v_0 = as_vector([sin(x)*cos(y)*cos(z), -cos(x)*sin(y)*cos(z), 0])
        w_0 = as_vector([-cos(x)*sin(y)*sin(z), -sin(x)*cos(y)*sin(z), 2*sin(x)*sin(y)*cos(z)])
        p_0 = 1/16*(cos(2*x) + cos(2*y))*(cos(2*z) + 2)

        if self.quad:
            v_init = project(v_0, V_v)
            w_init = project(w_0, V_w)
            p_init = project(p_0, V_p)
        else:
            v_init = interpolate(v_0, V_v)
            w_init = interpolate(w_0, V_w)
            p_init = interpolate(p_0, V_p)

        return [v_init, w_init, p_init]

    def __str__(self):
        return "Taylor_Green3D"
