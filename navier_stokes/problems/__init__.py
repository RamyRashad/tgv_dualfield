__all__=["TaylorGreen2D","TaylorGreen3D",
         "InviscidShearLayer",
         "InviscidVortexAdvection"]

from navier_stokes.problems.taylor_green2D import TaylorGreen2D
from navier_stokes.problems.taylor_green3D import TaylorGreen3D
from navier_stokes.problems.inviscid_shear_layer import InviscidShearLayer
from navier_stokes.problems.inviscid_vortex_advection import InviscidVortexAdvection

