import os
import numpy as np
import pandas as pd

def tecplot_reader(file, nb_var):
    arrays = []

    with open(file, 'r') as file:
        for line in file.readlines():
            if line.startswith('TITLE') or line.startswith('VARIABLES') or line.startswith('ZONE'):
                    continue
            else:
                arrays.append([float(s) for s in line.split()])
    arrays = np.concatenate(arrays)
    print(len(arrays))

    subarrays = np.array_split(arrays, nb_var)

    return subarrays

def read_dat_file(file_path):
    # Read data from the .dat file into a NumPy array
    data = np.loadtxt(file_path, delimiter=',')

    return data


def save_arr_as_csv(array_list,header_list,file_name):
    data_dict = {header: array for header, array in zip(header_list, array_list)}

    # Create a DataFrame from the dictionary
    df = pd.DataFrame(data_dict)

    df.to_csv(file_name)

if __name__ == "__main__":
    file_path = os.getcwd() + "/navier_stokes/post_processing/Re1600 Experiments/reference_data/"
    t_1_arr,E_1_arr,t_2_arr,E_2_arr = tecplot_reader(file_path+"Re=1600_Kinetic_energy.dat", 4)
    t_3_arr,dE_dt_1_arr,t_4_arr,dE_dt_2_arr = tecplot_reader(file_path+"Re=1600_Energy_dissipation_rate.dat", 4)

    data_arr = read_dat_file(file_path+"Brachet-Re=1600.dat")
    t_5_arr = data_arr[:,0]
    dE_dt_3_arr = data_arr[:, 1]


    array_list = [t_1_arr,E_1_arr,E_2_arr,dE_dt_1_arr,dE_dt_2_arr]
    header_list = ["t_1_arr","E_1_arr","E_2_arr","dE_dt_1_arr","dE_dt_2_arr"]

    save_arr_as_csv(array_list, header_list, file_path+"reference_data_1.csv")

    array_list = [t_5_arr,dE_dt_3_arr]
    header_list = ["t_5_arr","dE_dt_3_arr"]

    save_arr_as_csv(array_list, header_list, file_path + "reference_data_2.csv")

#


