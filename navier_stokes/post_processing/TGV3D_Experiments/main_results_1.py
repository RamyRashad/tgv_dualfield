import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import sys
if './' not in sys.path:
    sys.path.append('./')

from navier_stokes.utilities.utils import get_string_from_columns,get_cur_axis



if __name__ == '__main__':
    script_directory = os.path.dirname(os.path.realpath(__file__))
    Re = 1600
    Re_folder = '/Re' + str(Re)

    # Load the Referecne Data CSV file into a Pandas DataFrame
    ref_df_1 = pd.read_csv(script_directory + Re_folder + '/reference_data/reference_data_1.csv')

    ref_df_2 = pd.read_csv(script_directory + Re_folder + '/reference_data/reference_data_2.csv')

    t_Cab_Tyl_1 = ref_df_1["t_1_arr"]
    Ens_Cab_Tyl_1 = ref_df_1["dE_dt_1_arr"]
    E_Cab_Tyl_1 = ref_df_1["E_1_arr"]

    t_Brachet = ref_df_2["t_5_arr"]
    Ens_Brachet = ref_df_2["dE_dt_3_arr"]

    # Configuration of results
    folder_path = os.getcwd() + "/navier_stokes/results/taylor_green3d" + Re_folder + "_Exps/run_1/"
    V_dom = pow(2*np.pi,3)
    pol_deg = 4
    n_mesh = 16
    label_ext = str(n_mesh)+'p'+str(pol_deg)

    exp_path = get_string_from_columns(folder_path+'ph_experiments.csv', pol_deg, n_mesh)
    results_df = pd.read_csv(folder_path+exp_path+'/output_arrays.csv')

    # Time arrays
    t_pr = results_df["tspan_stag"]
    t_dl = results_df["tspan_int"]
    # Energy arrays
    E_pr = results_df["energy_pr"]/V_dom
    E_dl = results_df["energy_dl"]/V_dom
    # Enstrophy arrays
    Ens_pr = results_df["enstrophy_pr"]*2/(Re*V_dom)
    Ens_dl = results_df["enstrophy_dl"]*2/(Re*V_dom)


    # Results figure
    n_row = 2
    n_col = 2
    fig,axs = plt.subplots(n_row,n_col,figsize=(12.4,7.68))
    ref_label_1 = 'Cab & Tyl 256'
    ref_label_2 = 'Brachet et al'

    # Primal Energy
    ii=0
    axis = get_cur_axis(axs,ii,n_col)
    axis.plot(t_pr,E_pr,'b', label='df_pr_'+label_ext)
    axis.plot(t_Cab_Tyl_1,E_Cab_Tyl_1, 'r--', label=ref_label_1)
    axis.set_title('Primal', fontsize=14)
    axis.set_ylabel('Energy', fontsize=14)

    # Dual Energy
    ii+=1
    axis = get_cur_axis(axs,ii,n_col)
    axis.plot(t_dl,E_dl,'b', label='df_dl_'+label_ext)
    axis.plot(t_Cab_Tyl_1,E_Cab_Tyl_1, 'r--', label=ref_label_1)
    axis.set_title('Dual', fontsize=14)

    # Primal Enstrophy
    ii+=1
    axis = get_cur_axis(axs,ii,n_col)
    axis.plot(t_pr,Ens_pr,'b', label='df_pr_'+label_ext)
    axis.plot(t_Cab_Tyl_1,Ens_Cab_Tyl_1,'r--', label=ref_label_1)
    axis.plot(t_Brachet,Ens_Brachet, 'sk', label=ref_label_2)
    axis.set_ylabel('Enstrophy', fontsize=14)

    # Dual Enstrophy
    ii += 1
    axis = get_cur_axis(axs, ii, n_col)
    axis.plot(t_dl,Ens_dl,'b', label='df_dl_'+label_ext)
    axis.plot(t_Cab_Tyl_1,Ens_Cab_Tyl_1,'r--',label=ref_label_1)
    axis.plot(t_Brachet,Ens_Brachet, 'sk',  label=ref_label_2)

    # Turning on the grid for all subplots
    for ax in axs.flat:
        ax.grid(True)

    # Adding legend to all subplots
    for ax in axs.flat:
        ax.legend()

    # Adjusting layout for better appearance
    plt.tight_layout()

    plt.savefig(script_directory + Re_folder +'/figures/' +label_ext + '.png')
