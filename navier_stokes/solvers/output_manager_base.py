from firedrake import *
import numpy as np
import pandas as pd
import json


def split_arrays(dictionary):
    new_dictionary = {}

    for key, value in dictionary.items():
        if isinstance(value, np.ndarray) and value.ndim == 2 and value.shape[1] > 1:
            # If the array has more than 1 column, split it into separate arrays
            for idx in range(value.shape[1]):
                new_key = f"{key}_{idx+1}"
                new_dictionary[new_key] = value[:, idx]
        else:
            # Keep the array as is in the new dictionary
            new_dictionary[key] = value

    return new_dictionary


class OutputManagerBase:
    "Base class for all output managers."

    def __init__(self, problem,solver_param, fn_space_dim_dict):
        self.comm_rank = problem.comm.Get_rank()
        self.solver_param = solver_param
        self.fn_space_dim_dict = fn_space_dim_dict
        self.problem_param = problem.options
        self.problem_param.pop("comm")
        self.exact_sol = problem.exact
        self.n_t = problem.options["n_t_steps"]
        self.row_idx = 0
        self.col_idx = 0
        self.dimM = problem.dimM
        self.output_freq = solver_param["output_freq"]
        self.xi = SpatialCoordinate(problem.mesh)
        self.dim_w = 0
        if self.dimM == 3:
            self.dim_w = 3
        elif self.dimM == 2:
            self.dim_w = 1

        self.header_arr = []
        self.dim_arr =[]
        self.outputs_dict = {}

        if problem.exact:
            self.t_ex_c = Constant(0.0)
            self.exact_outputs = problem.init_outputs(self.t_ex_c)
        else:
            self.exact_outputs = None

        self.post_processor = None

        if self.solver_param["save_fig"]:
            self.results_path = self.solver_param['results_path']

    def update(self):
        "Update solver-specific outputs"
        pass

    def plot_results(self):
        if self.solver_param["save_fig"]:
            self.post_processor.plot_results(self.outputs_dict)

    def initialize_outputs_dict(self):
        for idx, header in enumerate(self.header_arr):
            self.init_array(header, self.dim_arr[idx])

    def init_array(self,label,dim):
        if dim ==1:
            self.outputs_dict[label] = np.zeros((self.n_t + 1,))
        else:
            self.outputs_dict[label] = np.zeros((self.n_t + 1,dim))

    def print_current_outputs(self):
        if self.comm_rank == 0:
            print(f'Current outputs at {self.row_idx-1} ')
            # Print the first element of each array
            for header in self.outputs_dict.keys():
                array = self.outputs_dict[header]
                print(f"{header}: {array[self.row_idx-1]}")

    def save_outputs(self):
        if self.solver_param["save_fig"]:
            new_dict = split_arrays(self.outputs_dict)
            results_df = pd.DataFrame(new_dict)
            results_df.to_csv(self.results_path + "/output_arrays.csv", index=False)

            fn_space_dim_df = pd.DataFrame(self.fn_space_dim_dict)
            fn_space_dim_df.to_csv(self.results_path + "/fn_space_dim.csv", index=False)

            total_dict = {**self.solver_param, **self.problem_param}
            with open(self.results_path + '/parameters_dict.txt', 'w') as total_dict_file:
                total_dict_file.write(json.dumps(total_dict))


    def add_output(self, quantity):
        self.outputs_dict[self.header_arr[self.col_idx]][self.row_idx] = quantity
        self.col_idx += 1

    def compute_energy(self, u):
        self.add_output(assemble(0.5 * dot(u, u) * dx))

    def compute_enstrophy(self, w):
        self.add_output(assemble(0.5 * dot(w, w) * dx))

    def compute_div_u_L2norm(self, u):
        divu_pr_0 = div(u) ** 2 * dx
        self.add_output(np.sqrt(assemble(divu_pr_0)))

    def compute_helicity(self, u, w):
        if self.dimM == 3:
            Hel_tot = assemble(dot(u, w) * dx)
        else:
            Hel_tot = 0
        self.add_output(Hel_tot)

    def compute_lin_mom(self,u):
        for kk in range(self.dim_arr[self.col_idx]):
            self.outputs_dict[self.header_arr[self.col_idx]][self.row_idx,kk] = assemble(u[kk] * dx)
        self.col_idx +=1

    def compute_ang_mom(self,u):
        if self.dim_arr[self.col_idx] == 3:
            ang_mom_tot = cross(u, self.xi)
            for kk in range(self.dim_arr[self.col_idx]):
                self.outputs_dict[self.header_arr[self.col_idx]][self.row_idx,kk] = assemble(ang_mom_tot[kk] * dx)
        else:
            self.outputs_dict[self.header_arr[self.col_idx]][self.row_idx] = assemble((u[0] * self.xi[1] - u[1] * self.xi[0]) * dx)
        self.col_idx +=1

    def compute_exact_energy(self):
        self.add_output(assemble(self.exact_outputs[3]))

    def compute_exact_enstrophy(self):
        self.add_output(assemble(self.exact_outputs[4]))

    def compute_exact_helicity(self):
        if self.dimM ==3:
            Hel = assemble(self.exact_outputs[5])
        else:
            Hel = 0
        self.add_output(Hel)
