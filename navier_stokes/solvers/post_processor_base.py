
class PostProcessorBase:
    "Base class for all post processors."

    def __init__(self,problem,solver_param):
        self.problem_param = problem.options
        self.solver_param = solver_param
        self.dimM = problem.dimM
        self.exact_sol = problem.exact

    def plot_results(self):
        pass