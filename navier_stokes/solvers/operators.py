from firedrake import *


def curl2D(v):
    return v[1].dx(0) - v[0].dx(1)


def rot2D(w):
    # 2D Rot i.e. rotated grad:  // ux = u.dx(0) // uy = u.dx(1) // as_vector((uy, -ux))
    return as_vector((w.dx(1), -w.dx(0)))


def f_M(chi_k, alpha_k):
    form = inner(chi_k, alpha_k) * dx
    return form


def f_C_pr(chi, alpha, dimM):
    if dimM == 3:
        form = inner(chi, curl(alpha)) * dx
    elif dimM == 2:
        form = inner(chi, curl(alpha)) * dx
        # form = inner(chi, curl2D(alpha)) * dx
    return form


def f_C_pr_tr(chi, alpha, dimM):
    return f_C_pr(alpha, chi, dimM)


def f_C_dl(chi, alpha, dimM):
    if dimM == 3:
        form = inner(chi, curl(alpha)) * dx
    elif dimM == 2:
        form = inner(chi, rot2D(alpha)) * dx
    return form


def f_C_dl_tr(chi, alpha, dimM):
    return f_C_dl(alpha, chi, dimM)


def f_D(chi, alpha):
    form = inner(chi, div(alpha)) * dx
    return form


def f_D_tr(chi, alpha):
    return f_D(alpha, chi)


def f_G(chi, alpha):
    form = inner(chi, grad(alpha)) * dx
    return form


def f_G_tr(chi, alpha):
    return f_G(alpha, chi)


def wcross_pr_form(chi, v, wT, dimM):
    if dimM == 3:
        form = inner(chi, cross(v, wT)) * dx
    elif dimM == 2:
        form = dot(chi, as_vector((v[1]*wT, -v[0]*wT))) * dx
    return form


def wcross_dl_form(chi, vT, w, dimM):
    if dimM == 3:
        form = inner(chi, cross(vT, w)) * dx
    elif dimM == 2:
        form = dot(chi, as_vector((vT[1]*w, -vT[0]*w))) * dx
    return form


# For 1st order tensors
# inner(u , v) = u_i v_i
# dot(u,v) = u_i v_i
# For 2nd order tensors
# inner(u_ij, v_kl) = u_ij v_ij
# dot(u_ij, v_kl) = u_ij v_jk


######################################################################
# For BVEC formulation
def f_N(chi, v, alpha):
    form = inner(chi, dot(v,nabla_grad(alpha))) * dx
    return form

def f_N_tr(chi, v, alpha):
    return f_N(alpha, v, chi)

def f_N_eta(chi, v, eta):
    form = inner(chi, dot(eta,v)) * dx
    return form

def f_CD(chi, alpha):
    form = inner(chi, tr(nabla_grad(alpha))) * dx
    return form


def f_CD_tr(chi, alpha):
    return f_CD(alpha, chi)


def f_skew_dual(chi,alpha, v):
    form = inner(chi,div(alpha*v))* dx - inner(div(chi*v),alpha)* dx
    return form


def skew_tilde(eta,dimM):
    form = None
    if dimM ==2:
        form = skew(eta)[1,0]
    elif dimM==3:
        eta_tld = skew(eta)
        form = as_vector((eta_tld[2,1],eta_tld[0,2],eta_tld[1,0]))
    return form


def tilde(omega,dimM):
    form = None
    if dimM ==2:
        form = as_matrix([[0, -omega],
                          [omega,0]])
    elif dimM==3:
        form = None
    return form