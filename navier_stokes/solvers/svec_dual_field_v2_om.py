from navier_stokes.solvers.output_manager_base import OutputManagerBase
from navier_stokes.solvers.svec_dual_field_v2_pp import PostProcessorSVECDualFieldv2
from firedrake import *
import numpy as np

class OutputManagerSVECDualFieldv2(OutputManagerBase):
    def __init__(self, problem,solver_param, fn_space_dim_dict):
        OutputManagerBase.__init__(self, problem, solver_param, fn_space_dim_dict)

        self.header_arr = ['energy_pr','enstrophy_pr', 'helicity_pr',
                           'divu_L2_pr', 'lin_mom_pr', 'ang_mom_pr',
                           'skewness_pr', 'flatness_pr',
                           'energy_dl','enstrophy_dl', 'helicity_dl',
                           'divu_L2_dl', 'lin_mom_dl', 'ang_mom_dl',
                           'skewness_dl', 'flatness_dl']
        self.dim_arr = [1, 1, 1, 1, self.dimM, self.dim_w, 1, 1,
                        1, 1, 1, 1, self.dimM, self.dim_w, 1, 1]

        if problem.exact:
            self.header_arr.extend(['energy_ex','enstrophy_ex','helicity_ex'])
            self.dim_arr.extend([1,1,1])

        self.initialize_outputs_dict()
        self.post_processor = PostProcessorSVECDualFieldv2(problem, solver_param)

    def update(self, t_act,
               u_pr_n32, w_pr_n32, p_pr_n1,
               u_pr_n1, w_pr_n1, p_pr_n12,
               u_dl_n1,w_dl_n1, p_dl_n12):
        self.col_idx = 0
        self.compute_energy(u_pr_n32)
        self.compute_enstrophy(w_pr_n32)
        self.compute_helicity(u_pr_n1, w_dl_n1)
        self.compute_div_u_L2norm(u_pr_n32)
        self.compute_lin_mom(u_pr_n32)
        self.compute_ang_mom(u_pr_n32)
        u_avg_pr = self.assemble_vel_average(u_pr_n32)
        self.compute_skewness(u_avg_pr)
        self.compute_flatness(u_avg_pr)

        self.compute_energy(u_dl_n1)
        self.compute_enstrophy(w_dl_n1)
        self.compute_helicity(u_dl_n1, w_pr_n1)
        self.compute_div_u_L2norm(u_dl_n1)
        self.compute_lin_mom(u_dl_n1)
        self.compute_ang_mom(u_dl_n1)
        u_avg_dl = self.assemble_vel_average(u_dl_n1)
        self.compute_skewness(u_avg_dl)
        self.compute_flatness(u_avg_dl)

        if self.exact_sol:
            self.t_ex_c.assign(t_act)
            self.compute_exact_energy()
            self.compute_exact_enstrophy()
            self.compute_exact_helicity()

        self.row_idx += 1

    def assemble_vel_average(self, v):
        arr = np.zeros((self.dimM, 4))
        for ii in range(self.dimM):
            for jj in range(4):
                arr[ii, jj] = assemble(v[ii] ** (jj + 1) * dx)
        return arr

    def compute_skewness(self, v_avg):
        numerator = v_avg[:, 2]
        divisor = pow(v_avg[:, 1], 3 / 2)
        S_u = np.divide(numerator,divisor,out=np.zeros_like(numerator), where=(divisor != 0))
        self.add_output(np.mean(S_u))

    def compute_flatness(self, v_avg):
        numerator = v_avg[:, 3]
        divisor = pow(v_avg[:, 1], 2)
        F_u = np.divide(numerator,divisor,out=np.zeros_like(numerator), where=(divisor != 0))
        self.add_output(np.mean(F_u))
