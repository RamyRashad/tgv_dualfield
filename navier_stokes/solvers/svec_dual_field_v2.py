import time
from navier_stokes.solvers.operators import *
from navier_stokes.utilities import utils
from tqdm import tqdm
import numpy as np
from navier_stokes.solvers.svec_dual_field_v2_om import OutputManagerSVECDualFieldv2
import pdb


def explicit_step_primal(dt_0, problem, solver_param, u_n,w_n, wT_n, V, p_nullspace, kappa):
    chi_u, chi_w, chi_p = TestFunctions(V)
    u_k, w_k, p_k = TrialFunctions(V)

    x_sol = Function(V)
    x_sol.sub(0).assign(u_n)
    x_sol.sub(1).assign(w_n)

    a1_form_vel = f_M(chi_u, u_k) + dt_0 * (f_G(chi_u, p_k) \
                                          - 0.5 * wcross_pr_form(chi_u, u_k, wT_n, problem.dimM) \
                                          + 0.5 *kappa * f_C_pr_tr(chi_u,  w_k, problem.dimM))
    a2_form_vor = f_M(chi_w, w_k) - f_C_pr(chi_w, u_k, problem.dimM)
    a3_form_p = f_G_tr(chi_p, u_k)

    b1_form = f_M(chi_u, u_n) + dt_0 * (0.5 * wcross_pr_form(chi_u, u_n, wT_n, problem.dimM) \
                                        - 0.5 * kappa *f_C_pr_tr(chi_u,  w_n, problem.dimM))

    V_nullspace = MixedVectorSpaceBasis(V, [V.sub(0), V.sub(1), p_nullspace])

    a_form = a1_form_vel + a2_form_vor + a3_form_p

    primal_prob = LinearVariationalProblem(a_form, b1_form, x_sol)
    primal_solver = LinearVariationalSolver(primal_prob, nullspace=V_nullspace, transpose_nullspace=V_nullspace,
                                          solver_parameters=solver_param)

    primal_solver.solve()

    return x_sol

def explicit_step_dual(dt_0, problem, solver_param, uT_n,wT_n, w_n, V, p_nullspace, kappa, pT_n = None):
    chi_u, chi_w, chi_p = TestFunctions(V)
    u_k, w_k, p_k = TrialFunctions(V)

    x_sol = Function(V)
    x_sol.sub(0).assign(uT_n)
    x_sol.sub(1).assign(wT_n)

    a1_form_vel = (f_M(chi_u, u_k) + 0.5 * dt_0 * kappa * f_C_dl(chi_u, w_k, problem.dimM)
                   - 0.5 * dt_0 * wcross_dl_form(chi_u, u_k, w_n, problem.dimM))

    a2_form_vor = f_M(chi_w, w_k) - f_C_dl_tr(chi_w, u_k, problem.dimM)
    a3_form_p = f_D(chi_p, u_k)

    b1_form = (f_M(chi_u, uT_n) + 0.5 * dt_0 * wcross_dl_form(chi_u, uT_n, w_n,problem.dimM)
               - 0.5 * dt_0 * kappa * f_C_dl(chi_u,wT_n,problem.dimM))

    V_nullspace = MixedVectorSpaceBasis(V, [V.sub(0), V.sub(1), p_nullspace])

    a_form = a1_form_vel + a2_form_vor + a3_form_p

    if pT_n is not None:
        b1_form = b1_form + 0.5*dt_0 * f_D_tr(chi_u,pT_n)
        a_form = a_form -0.5* dt_0 * f_D_tr(chi_u,p_k)
    else:
        a_form = a_form   - dt_0 * f_D_tr(chi_u,p_k)

    dual_prob = LinearVariationalProblem(a_form, b1_form, x_sol)
    dual_solver = LinearVariationalSolver(dual_prob, nullspace=V_nullspace, transpose_nullspace=V_nullspace,
                                          solver_parameters=solver_param)

    dual_solver.solve()

    return x_sol

def set_primal_finite_elements(problem, pol_deg):
    # Primal polynomial finite element families
    prob_mesh = problem.mesh
    V_1 = None
    V_2 = None
    if problem.quad:
        V_0 = FunctionSpace(prob_mesh, "Q", pol_deg)
        if problem.dimM == 3:  # Hexahedral mesh
            V_1 = FunctionSpace(prob_mesh, "NCE", pol_deg)
            V_2 = FunctionSpace(prob_mesh, "NCF", pol_deg)
        elif problem.dimM == 2:  # Quadrilateral mesh
            V_1 = FunctionSpace(prob_mesh, "RTCE", pol_deg)
            V_2 = FunctionSpace(prob_mesh, "DQ", pol_deg - 1)
    else:
        V_0 = FunctionSpace(prob_mesh, "P", pol_deg)
        if problem.dimM == 3:  # Tetrahedral mesh
            # V_1 = FunctionSpace(prob_mesh, "N1curl", pol_deg)
            # V_2 = FunctionSpace(prob_mesh, "RT", pol_deg)
            PE_1 = FiniteElement("N1curl", tetrahedron, pol_deg, variant="integral")
            PE_2 = FiniteElement("RT", tetrahedron, pol_deg, variant="integral")
            V_1 = FunctionSpace(prob_mesh, PE_1)
            V_2 = FunctionSpace(prob_mesh, PE_2)
        elif problem.dimM == 2:  # Triangular mesh
            PE_1 = FiniteElement("N1curl", tetrahedron, pol_deg, variant="integral")
            V_1 = FunctionSpace(prob_mesh, PE_1)
            V_2 = FunctionSpace(prob_mesh, "DP", pol_deg - 1)

    return V_1, V_2, V_0


def set_dual_finite_elements(problem, pol_deg):
    # Dual polynomial finite element families
    prob_mesh = problem.mesh
    # VT_n = FunctionSpace(prob_mesh, "DG", pol_deg - 1)
    VT_n1 = None
    VT_n2 = None
    if problem.quad:
        VT_n = FunctionSpace(prob_mesh, "DQ", pol_deg - 1)
        if problem.dimM == 3:  # Hexahedral mesh
            VT_n1 = FunctionSpace(prob_mesh, "NCF", pol_deg)
            VT_n2 = FunctionSpace(prob_mesh, "NCE", pol_deg)
        elif problem.dimM == 2:  # Quadrilateral mesh
            VT_n1 = FunctionSpace(prob_mesh, "RTCF", pol_deg)
            VT_n2 = FunctionSpace(prob_mesh, "Q", pol_deg)
    else:
        VT_n = FunctionSpace(prob_mesh, "DP", pol_deg - 1)
        if problem.dimM == 3:  # Tetrahedral mesh
            VT_n1 = FunctionSpace(prob_mesh, "RT", pol_deg)
            VT_n2 = FunctionSpace(prob_mesh, "N1curl", pol_deg)
        elif problem.dimM == 2:  # Triangular mesh
            VT_n1 = FunctionSpace(prob_mesh, "RT", pol_deg)
            VT_n2 = FunctionSpace(prob_mesh, "P", pol_deg)

    return VT_n1, VT_n2, VT_n


def compute_sol(solver_param, problem):
    # Implementation of the dual field formulation for generic navier stokes wih periodic boundary condition
    # Extract solver parameter dictionaries
    solver_param_primal = solver_param["primal"]
    solver_param_dual = solver_param["dual"]
    pol_deg = solver_param["pol_deg"]

    if solver_param["save_pvd"]:
        output_freq = solver_param["output_freq"]
        pvd_file_pr = File(solver_param["results_path"] + '/time_series_primal.pvd')
        pvd_file_dl = File(solver_param["results_path"] + '/time_series_dual.pvd')

    # Extract problem parameter dictionaries
    t_fin = problem.options["t_fin"]
    n_t = problem.options["n_t_steps"]
    kappa = Constant(problem.kappa)
    comm_rank = problem.comm.Get_rank()

    # Set Temporal mesh
    dt = problem.options["delta_t"]
    problem.dt = dt
    tvec_int = np.zeros((n_t + 1))
    tvec_stag = np.zeros((n_t + 1))

    # Execution time array
    exec_time_arr = np.zeros((n_t + 1))

    # Set mixed finite element spaces
    V_1, V_2, V_0 = set_primal_finite_elements(problem, pol_deg)
    VT_n1, VT_n2, VT_n = set_dual_finite_elements(problem, pol_deg)
    V_primal = MixedFunctionSpace([V_1, V_2, V_0])
    V_dual = MixedFunctionSpace([VT_n1, VT_n2, VT_n])
    # For these spaces, n denotes the spatial domain dimension, for other variables it represents the time step
    fn_space_dim_dict = None
    num_dof_arr = np.hstack((comm_rank, utils.sum_tuple(V_primal.dof_count), np.array(V_primal.dof_count),
                             utils.sum_tuple(V_dual.dof_count), np.array(V_dual.dof_count)))
    all_arrays = problem.comm.gather(num_dof_arr, root=0)
    if comm_rank == 0:
        fn_space_dim_arr = np.vstack(all_arrays)
        # print("Function Space dimensions [primal dof, dual dof, cells, faces, edges ,vertices]:")
        header_arr = np.array(['comm_rank',
                      'tot_dof_primal', 'dim_V_1', 'dim_V_2', 'dim_V_0',
                      'tot_dof_dual', 'dim_V_n1', 'dim_V_n2', 'dim_V_n',
                      'cells', 'faces', 'edges', 'vertices'])

        solver_param["n_dof_primal"] = str(np.sum(fn_space_dim_arr[:,1]))
        solver_param["n_dof_dual"] = str(np.sum(fn_space_dim_arr[:,5]))

        fn_space_dim_arr = np.hstack((fn_space_dim_arr, problem.mesh_info_arr))

        fn_space_dim_dict = dict(zip(header_arr, fn_space_dim_arr.T))

        # print(fn_space_dim_dict, flush=True)


    ####################################################################################################################
    # Set primal initial condition at t=0
    xprimal_0 = Function(V_primal, name="x_0 primal")
    x_pr_0 = problem.initial_conditions(V_1, V_2, V_0)

    u_pr_0 = x_pr_0[0]
    w_pr_0 = x_pr_0[1]
    p_pr_0 = x_pr_0[2]
    # This pressure that comes from the problem is the static pressure,
    # while we expect the pressure of the NS formulation to be dynamic pressure

    xprimal_0.sub(0).assign(u_pr_0)
    xprimal_0.sub(1).assign(w_pr_0)
    xprimal_0.sub(2).assign(p_pr_0)

    # Set dual initial condition at t=0
    xdual_0 = Function(V_dual, name="x_0 dual")
    x_dl_0 = problem.initial_conditions(VT_n1, VT_n2, VT_n)


    u_dl_0 = x_dl_0[0]
    w_dl_0 = x_dl_0[1]
    p_dl_0 = x_dl_0[2]
    # Same comment for pressure as above

    xdual_0.sub(0).assign(u_dl_0)
    xdual_0.sub(1).assign(w_dl_0)
    xdual_0.sub(2).assign(p_dl_0)

    ####################################################################################################################
    # Initialize post-processing variables
    om = OutputManagerSVECDualFieldv2(problem, solver_param, fn_space_dim_dict)
    om.update(0, u_pr_0, w_pr_0, p_pr_0,  u_pr_0, w_pr_0, p_pr_0, u_dl_0, w_dl_0, p_dl_0)
    om.print_current_outputs()
    ####################################################################################################################
    # Initialize the primal and dual systems at n=1/2 and n=0 respectively
    xdual_n = Function(V_dual, name="uT, wT at n, pT at n-1/2")
    xdual_n.sub(0).assign(u_dl_0)
    xdual_n.sub(1).assign(w_dl_0)
    xdual_n.sub(2).assign(p_dl_0)
    u_dl_n, w_dl_n, p_dl_12n = xdual_n.subfunctions

    p_pr_nullspace = VectorSpaceBasis(constant=True, comm=problem.mesh.comm)
    p_dl_nullspace = VectorSpaceBasis(constant=True, comm=problem.mesh.comm)

    xdual_n0 = explicit_step_dual(dt / 4, problem, solver_param_dual, u_dl_0,w_dl_0, w_pr_0, V_dual,p_dl_nullspace,kappa)
    u_dl_n0, w_dl_n0, p_dl_n0 = xdual_n0.subfunctions

    xprimal_n0 = explicit_step_primal(dt / 2, problem, solver_param_primal, u_pr_0,w_pr_0, w_dl_n0, V_primal,p_pr_nullspace,kappa)
    u_pr_n0, w_pr_n0, p_pr_n0 = xprimal_n0.subfunctions

    xprimal_n12 = xprimal_n0
    u_pr_n12, w_pr_n12, p_pr_n = xprimal_n12.subfunctions

    if comm_rank == 0:
        print("Explicit step solved",flush=True)
        # print("Explicit step approximated")

    ####################################################################################################################
    # FixMe: Now we should calculuate the postprocessing variables from the explicit initial step at n=1/2
    # Which should be only in case we want the plots of the primal and dual to be with different time arrays
    # An alternative is to interpolate primal at integer time steps

    ####################################################################################################################
    # Dual solver variables
    xdual_n1 = Function(V_dual, name="uT, wT at n+1, pT at n+1/2")
    # Initialize by initial condition as initial guess for iterative solvers
    xdual_n1.sub(0).assign(u_dl_0)
    xdual_n1.sub(1).assign(w_dl_0)
    xdual_n1.sub(2).assign(p_dl_0)
    u_dl_n1, w_dl_n1, p_dl_n12 = xdual_n1.subfunctions
    u_dl_n1.rename(name="uT at n+1")
    w_dl_n1.rename(name="wT at n+1")
    p_dl_n12.rename(name="pT at n+1/2")

    # Test and trial functions definition
    chi_u_dl, chi_w_dl, chi_p_dl = TestFunctions(V_dual)
    u_dl, w_dl, p_dl = TrialFunctions(V_dual)

    # Dual static operators
    a1_dual_st = f_M(chi_u_dl, u_dl) + 0.5 * dt * f_C_dl(chi_u_dl, kappa * w_dl, problem.dimM) - dt * f_D_tr(chi_u_dl,
                                                                                                             p_dl)
    a2_dual_st = f_M(chi_w_dl, w_dl) - f_C_dl_tr(chi_w_dl, u_dl, problem.dimM)
    a3_dual_st = f_D(chi_p_dl, u_dl)

    # Dual dynamic operators
    a_dual_dyn = - 0.5 * dt * wcross_dl_form(chi_u_dl, u_dl, w_pr_n12, problem.dimM)
    b1_dual = f_M(chi_u_dl, u_dl_n) + 0.5 * dt * wcross_dl_form(chi_u_dl, u_dl_n, w_pr_n12,
                                                                problem.dimM) - 0.5 * dt * f_C_dl(chi_u_dl,
                                                                                                  kappa * w_dl_n,
                                                                                                  problem.dimM)
    # Dual problem and solver
    a_dual = a1_dual_st + a2_dual_st + a3_dual_st + a_dual_dyn
    dual_prob = LinearVariationalProblem(a_dual, b1_dual, xdual_n1)

    Vdual_nullspace = MixedVectorSpaceBasis(V_dual, [V_dual.sub(0), V_dual.sub(1), p_dl_nullspace])
    dual_solver = LinearVariationalSolver(dual_prob, nullspace=Vdual_nullspace, transpose_nullspace=Vdual_nullspace,
                                          solver_parameters=solver_param_dual)
    ####################################################################################################################
    # Primal solver variables
    xprimal_n32 = Function(V_primal, name="u, w at n+3/2, p at n+1")
    # Initialize by initial condition as initial guess for iterative solvers
    xprimal_n32.sub(0).assign(u_pr_0)
    xprimal_n32.sub(1).assign(w_pr_0)
    xprimal_n32.sub(2).assign(p_pr_0)
    u_pr_n32, w_pr_n32, p_pr_n1 = xprimal_n32.subfunctions
    u_pr_n32.rename(name='u at n+1/2')
    w_pr_n32.rename(name="w at n+1/2")
    p_pr_n1.rename(name="p at n+1")

    xprimal_n1 = Function(V_primal, name="u, w at n+1, p at n+1/2")
    # Used termporarily to compute primal variables at integer time steps

    # Test and trial functions definition
    chi_u_pr, chi_w_pr, chi_p_pr = TestFunctions(V_primal)
    u_pr, w_pr, p_pr = TrialFunctions(V_primal)

    # Primal static operators
    a1_primal_st = f_M(chi_u_pr, u_pr) + 0.5 * dt * f_C_pr_tr(chi_u_pr, kappa * w_pr, problem.dimM) + dt * f_G(chi_u_pr,
                                                                                                               p_pr)
    a2_primal_st = f_M(chi_w_pr, w_pr) - f_C_pr(chi_w_pr, u_pr, problem.dimM)
    a3_primal_st = f_G_tr(chi_p_pr, u_pr)

    # Primal dynamic operators
    a_primal_dyn = -0.5 * dt * wcross_pr_form(chi_u_pr, u_pr, w_dl_n1, problem.dimM)
    b1_primal = f_M(chi_u_pr, u_pr_n12) + 0.5 * dt * wcross_pr_form(chi_u_pr, u_pr_n12, w_dl_n1, problem.dimM) \
                - 0.5 * dt * f_C_pr_tr(chi_u_pr, kappa * w_pr_n12, problem.dimM)

    # Primal problem and solver
    a_primal = a1_primal_st + a2_primal_st + a3_primal_st + a_primal_dyn
    primal_prob = LinearVariationalProblem(a_primal, b1_primal, xprimal_n32)

    Vprimal_nullspace = MixedVectorSpaceBasis(V_primal, [V_primal.sub(0), V_primal.sub(1), p_pr_nullspace])
    primal_solver = LinearVariationalSolver(primal_prob, nullspace=Vprimal_nullspace,
                                            transpose_nullspace=Vprimal_nullspace,
                                            solver_parameters=solver_param_primal)
    ####################################################################################################################

    # Save results of initial states
    if solver_param["save_pvd"]:
        pvd_file_pr.write(u_pr_n32, w_pr_n32, p_pr_n1, time=0.5 * float(dt))
        pvd_file_dl.write(u_dl_n1, w_dl_n1, p_dl_n12, time=0)

    # Time loop from 1 onwards
    time.sleep(1)  # wait for 1 sec
    # Only rank 0 creates the tqdm instance and prints progress
    if comm_rank == 0:
        progress_bar = tqdm(total=n_t, desc="Overall Progress")

    for ii in range(1, n_t + 1):
        start_time = time.time()  # Record the start time
        t_act = ii * float(dt)
        tvec_int[ii] = t_act
        t_half_act = t_act - 0.5 * float(dt)
        tvec_stag[ii] = t_half_act
        ######################################
        # Solve dual system for n+1
        dual_solver.solve()

        # utils.print_cond_num(a_dual)

        _u_dl_n1, _w_dl_n1, _p_dl_n12 = xdual_n1.subfunctions
        u_dl_n1.assign(_u_dl_n1)
        w_dl_n1.assign(_w_dl_n1)
        p_dl_n12.assign(_p_dl_n12)
        ##################################################################################################
        # Solve primal system at n_32
        primal_solver.solve()

        # utils.print_cond_num(a_primal)

        _u_pr_n32, _w_pr_n32, _p_pr_n1 = xprimal_n32.subfunctions
        u_pr_n32.assign(_u_pr_n32)
        w_pr_n32.assign(_w_pr_n32)
        p_pr_n1.assign(_p_pr_n1)
        ##################################################################################################
        # Compute post-processing variables
        xprimal_n1.assign(0.5 * (xprimal_n12 + xprimal_n32))
        u_pr_n1, w_pr_n1, p_pr_n12 = xprimal_n1.subfunctions
        om.update(t_act, u_pr_n32, w_pr_n32, p_pr_n1, u_pr_n1, w_pr_n1, p_pr_n12, u_dl_n1,
                               w_dl_n1, p_dl_n12)
        ##################################################################################################
        # Reassign dual, primal for next iteration
        xprimal_n12.assign(xprimal_n32)
        u_pr_n12.assign(u_pr_n32)
        w_pr_n12.assign(w_pr_n32)
        p_pr_n.assign(p_pr_n1)

        u_dl_n.assign(u_dl_n1)
        w_dl_n.assign(w_dl_n1)
        p_dl_12n.assign(p_dl_n12)

        # Save results
        if solver_param["save_pvd"] and ii % output_freq == 0:
            pvd_file_pr.write(u_pr_n32, w_pr_n32, p_pr_n1, time=t_half_act)
            pvd_file_dl.write(u_dl_n1, w_dl_n1, p_dl_n12, time=t_act)

        #  Update progress only in rank 0
        if comm_rank == 0:
            progress_bar.update(1)
            exec_time_arr[ii] = time.time() - start_time

    # print(tvec_int)
    # print(tvec_stag)

    if comm_rank == 0:
        progress_bar.close()
        om.outputs_dict["tspan_int"] = tvec_int
        om.outputs_dict["tspan_stag"] = tvec_stag
        om.outputs_dict["exec_time"] = exec_time_arr

        om.save_outputs()
        om.plot_results()
        return om.outputs_dict
    else:
        return None
