from navier_stokes.solvers.post_processor_base import PostProcessorBase
import matplotlib.pyplot as plt
from navier_stokes.utilities.utils import get_cur_axis


class PostProcessorSVECDualFieldv2(PostProcessorBase):
    "Base class for all post processors."

    def __init__(self,problem,solver_param):
        PostProcessorBase.__init__(self,problem,solver_param)

    def plot_dualfield_quantity(self, axis, tvec_stag, tvec_int, x_pr, x_dl, x_ex, title):
        axis.plot(tvec_stag, x_pr, 'b', label="primal")
        axis.plot(tvec_int, x_dl, 'r', label="dual")
        if self.exact_sol:
            axis.plot(tvec_int, x_ex, 'g', label="exact")
        axis.legend()
        axis.grid(True)
        axis.set_title(title)

    def plot_results(self,outputs):
        # Results figure
        if self.dimM == 2:
            n_row = 2
            n_col = 4
        else:
            n_row = 2
            n_col = 4
        fig, axs = plt.subplots(n_row, n_col, figsize=(19.2, 12.78))

        tvec_int = outputs["tspan_int"]
        tvec_stag = outputs["tspan_stag"]

        if self.exact_sol:
            H_ex = outputs["energy_ex"]
            E_ex = outputs["enstrophy_ex"]
            Hel_ex = outputs["helicity_ex"]
        else:
            H_ex = None
            E_ex = None
            Hel_ex = None

        # Primal Energy
        ii = 0
        axis = get_cur_axis(axs, ii, n_col)
        self.plot_dualfield_quantity(axis,  tvec_stag, tvec_int, outputs["energy_pr"], outputs["energy_dl"], H_ex, "Energy")

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        self.plot_dualfield_quantity(axis,  tvec_stag, tvec_int, outputs["enstrophy_pr"], outputs["enstrophy_dl"], E_ex, "Enstrophy")

        if self.dimM == 3:
            ii += 1
            axis = get_cur_axis(axs, ii, n_col)
            axis.plot(tvec_int, outputs["helicity_pr"], 'b', label="primal")
            axis.plot(tvec_int, outputs["helicity_dl"], 'r', label="dual")
            if self.exact_sol:
                axis.plot(tvec_int, Hel_ex, 'g', label="exact")
            axis.legend()
            axis.grid(True)
            axis.set_title("Helicity")

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        axis.plot(tvec_stag, outputs["divu_L2_pr"], 'b', label="primal")
        axis.plot(tvec_int, outputs["divu_L2_dl"], 'r', label="dual")
        axis.set_title("L2 norm div(u)")
        axis.grid(True)

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        axis.plot(tvec_stag, outputs["lin_mom_pr"][:,0], 'b', label="primal - x")
        axis.plot(tvec_int, outputs["lin_mom_dl"][:,0], '--b', label="dual - x")
        axis.plot(tvec_stag, outputs["lin_mom_pr"][:,1], 'r', label="primal - y")
        axis.plot(tvec_int, outputs["lin_mom_dl"][:,1], '--r', label="dual - y")
        if self.dimM == 3:
            axis.plot(tvec_stag, outputs["lin_mom_pr"][:,2], 'g', label="primal - z")
            axis.plot(tvec_int, outputs["lin_mom_dl"][:,2], '--g', label="dual - z")
        axis.set_title("Linear momentum(u)")
        axis.legend()
        axis.grid(True)

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        if self.dimM ==2:
            axis.plot(tvec_stag, outputs["ang_mom_pr"], 'g', label="primal - z")
            axis.plot(tvec_int, outputs["ang_mom_dl"], '--g', label="dual - z")

        elif self.dimM == 3:
            axis.plot(tvec_stag, outputs["ang_mom_pr"][:,0], 'b', label="primal - x")
            axis.plot(tvec_int, outputs["ang_mom_dl"][:,0], '--b', label="dual - x")

            axis.plot(tvec_stag, outputs["ang_mom_pr"][:,1], 'r', label="primal - y")
            axis.plot(tvec_int, outputs["ang_mom_dl"][:,1], '--r', label="dual - y")

            axis.plot(tvec_stag, outputs["ang_mom_pr"][:,2], 'g', label="primal - z")
            axis.plot(tvec_int, outputs["ang_mom_dl"][:,2], '--g', label="dual - z")

        axis.set_title("Angular momentum(u x xi)")
        axis.legend()
        axis.grid(True)

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        axis.plot(tvec_stag, outputs["skewness_pr"], 'b', label="primal")
        axis.plot(tvec_int, outputs["skewness_dl"], '--b', label="dual")
        axis.legend()
        axis.grid(True)
        axis.set_title("Velocity Skewness S(u)")

        ii += 1
        axis = get_cur_axis(axs, ii, n_col)
        axis.plot(tvec_stag, outputs["flatness_pr"], 'b', label="primal")
        axis.plot(tvec_int, outputs["flatness_dl"], '--b', label="dual")
        axis.legend()
        axis.grid(True)
        axis.set_title("Velocity Flatness F(u)")

        # Add header
        pol_deg = self.solver_param["pol_deg"]
        dof_primal = self.solver_param["n_dof_primal"]
        dof_dual = self.solver_param["n_dof_dual"]

        n_el = self.problem_param["n_mesh_el"]
        n_t = self.problem_param["n_t_steps"]
        delta_t = self.problem_param["delta_t"]

        fig.suptitle(str(n_el) + 'p' + str(pol_deg) + '   |   n_dofs: ' + str(dof_primal) + "|" + str(dof_dual) +
                        "  |   n_t: " + str(n_t) + '   |   dt: ' + str(delta_t))

        # Adjusting layout for better appearance
        plt.tight_layout()

        plt.savefig(self.solver_param["results_path"] + '/overall_results_v2.png', dpi=100)
