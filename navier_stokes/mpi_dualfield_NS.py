import sys
if './' not in sys.path:
    sys.path.append('./')
import os
os.environ["OMP_NUM_THREADS"] = "1"
from firedrake import COMM_WORLD
from utilities import utils
from problems import TaylorGreen2D,TaylorGreen3D,InviscidShearLayer,InviscidVortexAdvection
from solvers import svec_dual_field_v2

# Problem IDs
_TaylorGreen2D = 1
_TaylorGreen3D = 2
_InviscidVortexAdvection = 3
_InviscidShearLayer = 4


class NavierStokesSimulation:
    def __init__(self, problem_dict, solver_dict, path_ext = ""):
        self.comm_rank = problem_dict["comm"].Get_rank()
        self.sim_print("##################### Start of Simulation ##############################")

        # 1. Choose problem
        prob_num = problem_dict["prob_num"]
        if prob_num == 1:
            problem = TaylorGreen2D(problem_dict)
        elif prob_num == 2:
            problem = TaylorGreen3D(problem_dict)
        elif prob_num == 3:
            problem = InviscidVortexAdvection(problem_dict)
        elif prob_num == 4:
            problem = InviscidShearLayer(problem_dict)
        else:
            problem = None

        self.sim_print(f"Problem {str(problem)} has been initialized")

        # 2. Define Solver parameters
        if solver_dict["solver_param_primal"] == "direct":
            solver_param_primal = {"ksp_type": "preonly", "pc_type": "lu", "mat_type": "aij",
                                   "pc_factor_mat_solver_type": "mumps"}
        elif solver_dict["solver_param_primal"] == "iterative":
            solver_param_primal = {"ksp_type": "gmres", "ksp_gmres_restart": 100,
                                   "ksp_rtol": 1e-7, 'pc_type': 'bjacobi'}
        solver_param_dual = solver_param_primal

        solver_dict["primal"] = solver_param_primal
        solver_dict["dual"] = solver_param_dual

        # 3. Set results path
        _save_fig = solver_dict["save_fig"]
        _save_pvd = solver_dict["save_pvd"]
        if _save_fig or _save_pvd:
            self.results_path = os.path.normpath(utils.results_prefix(str(problem),path_ext))
        else:
            self.results_path = None
        solver_dict["results_path"] = self.results_path

        # 4. Run solver
        try:
            n_mesh = problem_dict["n_mesh_el"]
            pol_deg = solver_dict["pol_deg"]
            self.sim_print(f"Starting solver with n_mesh = {n_mesh} and po_deg = {pol_deg}")
            self.results = svec_dual_field_v2.compute_sol(solver_dict, problem)
            self.sim_print("Solver finished successfully !")
            self.sim_print("##################### End of Simulation ##############################")
        except Exception as e:
            self.sim_print(f"\n An error occurred: {e}")

    def sim_print(self,text):
        if self.comm_rank ==0:
            print(text,flush=True)


if __name__ == '__main__':
    t_f = float(os.environ.get("t_f"))
    f_sol = int(os.environ.get("f_sol"))  # Hz
    n_t = int(t_f * f_sol)
    _problem_dict = {"prob_num": int(os.environ.get("prob_num")),
                     "n_t_steps": n_t,
                     "n_mesh_el": int(os.environ.get("n_mesh_el")),
                     "delta_t": 1.0 / f_sol,
                     "t_fin": t_f,
                     "mesh_type": "quad"}
    _solver_dict = {"save_fig": os.environ.get("save_fig").lower() == "true",
                    "save_pvd": os.environ.get("save_pvd").lower() == "true",
                    "pol_deg": int(os.environ.get("pol_deg")),
                    "output_freq": int(f_sol / 5),
                    "solver_param_primal": "iterative"}

    _problem_dict["comm"] = COMM_WORLD

    utils.extract_env_variables(os.environ,["SLURM_NNODES", "SLURM_TASKS_PER_NODE", "SLURM_JOB_ID"],_problem_dict)

    NavierStokesSimulation(_problem_dict, _solver_dict)