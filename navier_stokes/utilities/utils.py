import os
os.environ["OMP_NUM_THREADS"] = "1"
from datetime import datetime
import pytz
CET = pytz.timezone('Europe/Amsterdam')

from firedrake import assemble
import numpy as np
import pandas as pd

def results_prefix(prob_name, ext = ""):
    "Return file prefix for output files"
    p = prob_name.split(".")[-1].lower()
    script_dir = os.path.dirname(__file__)
    results_dir = os.path.join(script_dir, '../results/'+p)
    date_time_obj = datetime.now(CET)
    time_stamp = date_time_obj.strftime("%Y_%m_%d/%H_%M_%S")
    prefix = results_dir + '/' + time_stamp + ext
    os.makedirs(prefix, exist_ok=True)
    return prefix

def sum_tuple(tuple):

    sum = 0
    for item in tuple:
        sum += item

    return sum

def extract_env_variables(source_dict, keys, target_dict):
    for key in keys:
        if key in source_dict:
            target_dict[key] = source_dict[key]



def get_string_from_columns(input_csv, col1_value, col2_value):
    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(input_csv)

    # Filter the DataFrame based on the input values in columns 1 and 2
    filtered_df = df[(df['pol_deg'] == col1_value) & (df['n_mesh_el'] == col2_value)]

    # Check if any rows match the input values
    if not filtered_df.empty:
        # Get the string from column 3 for the first matching row
        result_string = filtered_df.iloc[0]['results_path']
        return result_string
    else:
        return "No matching row found."


def get_cur_axis(axis, index, num_cols):
    """
    Given an index and the number of columns in a grid,
    returns the row and column indices of the subplot.
    """
    row_index, col_index = divmod(index, num_cols)
    return axis[row_index, col_index]