#!/bin/bash

## Define variables
PROJECT_NAME="ns_dualfield_svec"
CONTAINER_PATH="/home/firedrake/$PROJECT_NAME"
DOCKER_IMAGE="ramyrashad/custom-firedrake-portwings"
CONTAINER_NAME="portwings_container"

## Run Docker container
echo "Starting Docker Container in Interactive Model, after the simulation ends Type <exit> to leave"
echo "The Present Working Directory is: /$(pwd)"
docker run -i --name "$CONTAINER_NAME" --volume "/$(pwd)":/"$CONTAINER_PATH"  "$DOCKER_IMAGE" bash

# Remove docker container for future use
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME

read -p "Press any key to confirm exit..."
