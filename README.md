# NS_dualfield_svec

This repository implements the dual-field structure preserving discretization method [1] for incompressible Navier-Stokes (**NS**) 
equations based on scalar-valued exterior calculus (**SVEC**). Hence, the name **NS_dualfield_svec**. 
This discretization scheme is currently applicable to periodic spatial domains only.
It has been tested on benchmark problems in 2D and 3D periodic domains. Namely:
1) Taylor-Green vortex problem (TGV), 
2) Inviscid vortex advection problem (IVA),
3) Inviscid shear layer problem (ISA).

Check out Ramy Rashad's [website](https://ramyrashad.com/index.php/2024/03/21/dual-field-discretization-of-navier-stokes-equations-scalar-valued-exterior-calculus/) for a demonstration of the numerical experiments.
The code has been developed within the [Portwings](http://www.portwings.eu) project and is based on Firedrake with parallelization capabilities.


## Installation
This is a Python project that utilizes Docker for containerization and Git Bash for shell scripting.
The project has been tested on Windows and Linux.
Before getting started, ensure you have the following installed on your system:
- Git Bash (Windows users only)
- Docker
- Code Repository
- Firedrake Docker Image

### Installing Git Bash

1. Visit the Git website: [https://git-scm.com/downloads](https://git-scm.com/downloads)
2. Download the Git for Windows installer.
3. Double-click the downloaded installer and follow the installation instructions.
4. During installation, select the option to add Git Bash to your PATH environment.
5. Once installed, you can launch Git Bash from the Start menu.

### Installing Docker

1. Visit the Docker website: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
2. Download the Docker Desktop installer for Windows/Linux.
3. Double-click the downloaded installer and follow the installation instructions.
4. Once installed, launch Docker Desktop from the Start menu.
5. Docker should now be running on your system.

### Installing NS_dualfield_svec

1. Clone this repository to your local machine using Git Bash/Command Terminal:
```
git clone https://gitlab.com/RamyRashad/ns_dualfield_svec.git
```
2. Pull our custom & pre-built Firedrake image from the docker hub:
```
docker pull ramyrashad/custom-firedrake-portwings
```

## Basic Usage
Thanks to Docker, running the code on a local machine is very easy.

1. Navigate to the project directory 'ns_dualfield_svec' using Git Bash or Right-Click project directory and choose 
'Open Git Bash here'.
2. Start a docker container
```
chmod +x run_docker_container.sh
./run_docker_container.sh
```
3. Inside the docker container, named '**portwings_container**', you are in a python environment with firedrake installed.
- Navigate to the project directory
```
cd ns_dualfield_svec
```
- Set the simulation parameters in [run_simulation.sh](run_simulation.sh)
- Run the simulation
```
chmod +x run_simulation.sh
./run_simulation.sh
```
- The results of the simulation can be found in the [results folder](navier_stokes/results)

## Post-processing results
- The results are saved in the following format '**/results/prob_name/date/time**'.
- To visualize the results saved in '**time_series_primal.pvd**' and '**time_series_dual.pvd**', 
open these files in [Paraview](https://www.paraview.org/download/).
- In the folders [taylor_green3d/Re400_Exps](navier_stokes/results/taylor_green3d/Re400_Exps) and
[taylor_green3d/Re1600_Exps](navier_stokes/results/taylor_green3d/Re1600_Exps) the raw results of the 
simulations produced from the [UTwente Cluster](https://hpc.wiki.utwente.nl/eemcs-hpc) and used in the DLES paper [2] can be found.
- The post-processed results that include the reference data of 'Brachet et al' and 'Caban & Tyliszczak'
can be found [here](navier_stokes/post_processing/TGV3D_Experiments).

## Authors and acknowledgment
Contributers to this project are:
Ramy Rashad, Yi Zhang, Andrea Brugnoli, and Stefano Stramigioli.

We acknowledge Lena Caban, Artur Tyliszczak, Erwin Luesink, and Bernard Geurts for their collaboration and providing 
the reference data.

This repository is maintained by Ramy Rashad (ramy.abdelmonem@gmail.com).

## References

[1] Zhang, Y., Palha, A., Gerritsma, M., & Rebholz, L. G. (2022). A mass-, kinetic energy-and helicity-conserving mimetic
dual-field discretization for three-dimensional incompressible Navier-Stokes equations, part I: Periodic domains.
Journal of Computational Physics, 451, 110868.

[2] Luesink, E., Caban, L., Rashad, R., Zhang Y., Stramigioli, S., Tyliszczak A., Guerts B. (2024) How important
is structure-preserving discretization for the prediction of Taylor-Green Turbulence? Direct and Large-Eddy Simulation 14.